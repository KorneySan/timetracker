export interface TrackItem {
    id: number;
    start_time: number;
    pause_time: number;
    pause_sum: number;
    stop_time: number;
}
