import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import * as moment from 'moment';
import { TrackService } from '../../services';
import { TrackItem } from '../../models/time.model';

@Component({
  selector: 'track-time',
  templateUrl: './track-time.component.html',
  styleUrls: ['./track-time.component.scss'],
  animations: [
    trigger('trackState', [
      state('normal', style({
        //transform: 'translateY(0)',
        height: '0px'
      })),
      state('open', style({
        //transform: 'translateY(-400px)',
        height: '200px'
      })),
      transition('normal => open',  animate(500)), // <=> для анимации с одинаковым временем в обоих случаях
      transition('open => normal', animate(700))
    ])
  ]
})

export class TrackTimeComponent implements OnInit {

  public state: string = 'normal';
  private realState: string = 'normal';
  public trackState: string = 'stop';
  public startTime: number = null;
  public currentTime: number = null;
  public pauseTime: number = null;
  private timer: any = null;

  constructor(private trackService: TrackService) { }

  ngOnInit() {
    this.loadStartTime();
    if (this.trackService.isHaveOpenTrack()) {
      this.trackService.startTrack();
      this.startTime = this.trackService.track.start_time;
      this.startTimer();
      this.trackState = 'start';
    }
  }

  public get isOpened(): boolean {
    return (this.realState == 'open');
  }

  public toggleState(): void {
    this.state == 'normal' ? this.state = 'open' : this.state = 'normal';
  }

  public animationEnded(event): void {
    //console.log(event);
    this.realState = this.state;
  }

  public toggleStart(): void {
    if (this.trackState=='stop') {
      this.trackState='start';
      this.Started();
    } 
    else {
      this.trackState='stop';
      this.Stopped();
    }
  }

  public togglePause(): void {
    if (this.trackState=='pause') {
      this.trackState='start';
      this.Resumed();
    }
    else {
      if (this.trackState=='start') {
        this.trackState='pause';
        this.Paused();
      }
    }
  }

  private loadStartTime(): void {
    if (this.trackService.track && this.trackService.track.start_time && !this.trackService.track.stop_time) {
      this.startTime = this.trackService.track.start_time;
      this.startTimer();
    }
  }

  private startTimer(): void {
    this.currentTime=moment().unix();
    this.timer = setInterval(()=> {
      this.currentTime=moment().unix();
    }, 1000);
  }

  private stopTimer(): void {
    clearInterval(this.timer);
    this.timer = null;
  }

  private Started(): void {
    this.trackService.startTrack();
    this.loadStartTime();
  }

  private Stopped(): void {
    this.stopTimer();
    this.trackService.stopTrack();
    this.currentTime = 0;
  }

  private Paused(): void {
    this.pauseTime = moment().unix();
    this.trackService.pauseTrack();
  }

  private Resumed(): void {
    this.trackService.resumeTrack();
    this.pauseTime = null;
  }

  public get pauseSum(): number {
    return this.trackService.pauseSum;
  }
}
