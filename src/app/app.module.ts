import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, PreloadAllModules } from '@angular/router';

import { AppComponent } from './app.component';
import { PagesModule } from './pages/pages.module';
import { routes } from './app-routes';
import { TimerService, AuthGuardService, ModalService, AuthService, TrackService } from './services';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}),
    PagesModule
  ],
  providers: [
    TimerService,
    AuthGuardService,
    ModalService,
    AuthService,
    TrackService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
