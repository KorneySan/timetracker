import { Injectable } from '@angular/core';

@Injectable()

export class AuthService {

  constructor() { }

  doAuthorisation(user: User): void {
    if (this.isUserValid(user)) {
      localStorage.setItem('isAuth', '1');
    }
    else {
      localStorage.setItem('isAuth', '0');
    }
  }

  private isUserValid(user: User): boolean {
    return (user.email=='1@qqq.ww' && user.password=='12345678');
  }

  isAuth(): boolean {
    const auth = localStorage.getItem('isAuth');
    return (+auth==1);
  }

}

interface User {
  email: string;
  password: string;
}
