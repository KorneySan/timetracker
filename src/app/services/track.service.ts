import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { TrackItem } from '../models/time.model';

@Injectable()

export class TrackService {

  public track: TrackItem = null;

  constructor() { }

  private loadData(): TrackItem[] {
    const trackArray: TrackItem[] = JSON.parse(localStorage.getItem('trackArray'));
    return trackArray;
  }

  private saveData(track: TrackItem): void {
    let trackArray: TrackItem[] = this.loadData();
    if (!trackArray) trackArray = [];
    trackArray.push(this.track);
    //update id
    const index = trackArray.indexOf(this.track);
    this.track.id = index;
    trackArray.splice(index, 1, this.track);
    //
    this.saveTrackArray(trackArray);
  }

  private saveTrackArray(trackArray: TrackItem[]): void {
    if (trackArray) {
      localStorage.setItem('trackArray', JSON.stringify(trackArray));
    }
  }

  public startTrack(): void {
    /*
    const trackArray: TrackItem[] = this.loadData();
    const notClosedTrack: TrackItem = trackArray.find(track => track.stop_time === null);
    */
   const notClosedTrack = this.getOpenTrack();
   if (notClosedTrack) {
      this.track = notClosedTrack;
    }
    else {
      this.track = {
        id: 0,
        start_time: moment().unix(),
        pause_time: null,
        pause_sum: null,
        stop_time: null
      };
      this.saveData(this.track);
    }
  }

  public stopTrack(): void {
    const trackArray: TrackItem[] = this.loadData();
    /*
    const notClosedTrack: TrackItem = trackArray.find(track => track.stop_time === null);
    */
    if (!this.track) this.track = this.getOpenTrack();
    const index = trackArray.indexOf(this.track);
    this.track.stop_time = moment().unix();
    trackArray.splice(index, 1, this.track);
    this.saveTrackArray(trackArray);
    /*
    this.track.stop_time = moment().unix();
    this.saveData(this.track);
    */
  }

  public isHaveOpenTrack(): boolean {
    /*
    const trackArray: TrackItem[] = this.loadData();
    const notClosedTrack: TrackItem = trackArray.find(track => track.stop_time === null);
    return !!(notClosedTrack);
    */
    return !!this.getOpenTrack();
  }

  public getOpenTrack(): TrackItem {
    const trackArray: TrackItem[] = this.loadData();
    if (trackArray) {
      const notClosedTrack: TrackItem = trackArray.find(track => track.stop_time === null);
      return notClosedTrack;
    }
    return null;
  }

  public pauseTrack(): void {
    const trackArray: TrackItem[] = this.loadData();
    if (!this.track) {
      this.track = this.getOpenTrack();
    }
    const index = trackArray.indexOf(this.track);
    //set pause time
    this.track.pause_time = moment().unix();
    //
    if (index<0) trackArray.push(this.track);
    else trackArray.splice(index, 1, this.track);
    this.saveTrackArray(trackArray);
  }

  public resumeTrack(): void {
    const trackArray: TrackItem[] = this.loadData();
    if (!this.track) {
      this.track = this.getOpenTrack();
    }
    const index = trackArray.indexOf(this.track);
    //calculate pause sum
    const currentTime = moment().unix();
    if (this.track.pause_sum) this.track.pause_sum += currentTime-this.track.pause_time;
    else this.track.pause_sum = currentTime-this.track.pause_time;
    this.track.pause_time = null;
    //
    if (index<0) trackArray.push(this.track)
    else trackArray.splice(index, 1, this.track);
    this.saveTrackArray(trackArray);
  }

  public get pauseSum(): number {
    if (this.track) return this.track.pause_sum;
    else return 0;
  }

}
