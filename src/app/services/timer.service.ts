import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Subject } from 'rxjs';

@Injectable ()

export class TimerService {
    //public longTimerEmitter: Subject<number> = new Subject();
    public TimerEmitter: Subject<number> = new Subject();
    private readonly longTime: number = 30000;
    private readonly second: number = 1000;

    constructor() {
        //this.startLongTimer();
        this.startTimer();
    }

    get currentTime(): number {
        return moment().unix();
    }
    /*
    private startLongTimer(): void {
        setInterval(() => {
            this.longTimerEmitter.next(this.currentTime);
        }, this.longTime);
    }
    */
    
    private startTimer(): void {
        setInterval(() => {
            this.TimerEmitter.next(this.currentTime);
        }, this.second);
    }
    
}