import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ModalService } from './modal.service';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private modalService: ModalService, private authService: AuthService) { }

  canActivate() {
    /*
    this.router.navigate(['home']);
    this.modalService.showModal();
    return false;
    */
   if (this.authService.isAuth()) {
     this.modalService.closeModal();
     return true;
   }
   else {
    this.router.navigate(['home']);
    this.modalService.showModal();
    return false;
   }
  }

}
