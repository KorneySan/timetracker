import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'timeseconds'
})
export class TimesecondsPipe implements PipeTransform {
  transform(value: number): string {
    moment.locale('ru');
    if (value) return moment.unix(value).utcOffset(0).format('HH:mm:ss');
    else return '00:00:00';
  }
}
