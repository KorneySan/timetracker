import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({name: 'time'})
export class TimePipe implements PipeTransform {
    transform(value: number): string {
        moment.locale('ru');
        return moment.unix(value).format('HH:mm');
    }
}