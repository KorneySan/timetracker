import { FormControl } from "@angular/forms";

export function emailCorrectValidator(control: FormControl) {
    const value = control.value;
    const emailRegex = /^([A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$)/i;
    const result = emailRegex.test(value);
    if (result) return null;
    else {
        return {
            "emailCorrectValidator": {
                valid: false
            }
        }
    }
}
