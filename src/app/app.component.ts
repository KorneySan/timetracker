import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ModalService } from './services';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'TimeTracker';

  public isShowModal: boolean = false;

  constructor(private router: Router, private modalService: ModalService) {}

  ngOnInit(): void {
    this.modalService.modalState.subscribe(state => {
      this.isShowModal = state;
    });
  }

  public openAccount(): void {
    //by click
    this.router.navigate(['account']);
  }

}
