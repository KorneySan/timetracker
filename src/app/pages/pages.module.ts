import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { 
    HomePageComponent, 
    //AccountPageComponent,
    ModalLoginComponent,
    TrackTimeComponent,
    TrackListComponent
} from './'
import { TimePipe, TimesecondsPipe } from '../pipes';

@NgModule({
    declarations: [
        HomePageComponent,
        //AccountPageComponent,
        ModalLoginComponent,
        TimePipe,
        TrackTimeComponent,
        TimesecondsPipe,
        TrackListComponent
    ],
    imports: [ 
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule
        ],
    exports: [
        HomePageComponent,
        //AccountPageComponent,
        ModalLoginComponent,
        TimePipe,
        TrackTimeComponent,
        TimesecondsPipe,
        TrackListComponent
    ],
    providers: [],
})
export class PagesModule {}
