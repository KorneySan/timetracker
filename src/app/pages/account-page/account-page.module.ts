import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AccountPageComponent } from './account-page.component';

@NgModule({
    declarations: [
        AccountPageComponent
    ],
    imports: [ 
        CommonModule,
        RouterModule.forChild([
            {path: '', component: AccountPageComponent}
        ])
    ],
    exports: [

    ],
    providers: [

    ],
})
export class AccountPageModule {}