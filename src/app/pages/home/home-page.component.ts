import { Component, OnInit } from '@angular/core';
import { TimerService } from '../../services'
import * as moment from 'moment';

@Component({
    selector: 'home-page',
    templateUrl: './home-page.component.html',
    styleUrls: ['./home-page.component.scss']
})

export class HomePageComponent implements OnInit {
    constructor(private ts: TimerService) { }

    public currentTime: number;

    ngOnInit(): void { 
        this.ts.TimerEmitter.subscribe(currentTime => {
            this.currentTime = currentTime;
        });
    }

    public readyToHome(): boolean {
        return false; //this.currentTime > moment("20:30");
    }
}
