import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalService, AuthService } from '../../services';
import { emailCorrectValidator } from '../../validators/email.validator';
import { Router } from '@angular/router';

@Component({
  selector: 'modal-login',
  templateUrl: './modal-login.component.html',
  styleUrls: ['./modal-login.component.scss']
})
export class ModalLoginComponent implements OnInit {

  //@Output() modalEvent: EventEmitter<boolean> = new EventEmitter();

  public loginForm: FormGroup;
  public email: FormControl;
  public password: FormControl;

  constructor(private modalService: ModalService, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.createFormFields();
    this.createForm();
  }

  /*
  public closeWindow(): void {
    this.modalEvent.emit(true);
  }
  */

  private createFormFields(): void {
    this.email = new FormControl('', [Validators.required, emailCorrectValidator /* Validators.pattern(/^([A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$)/i)*/]);
    this.password = new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(15)]);
  }

  private createForm(): void {
    this.loginForm = new FormGroup({
      email: this.email,
      password: this.password
    });
  }

  public closeModal():void {
    this.modalService.closeModal();
  }

  public stopProp(event: MouseEvent): void {
    event.stopPropagation();
  }

  public sendData():void {
    console.log(this.loginForm.value);
    this.authService.doAuthorisation({email: this.loginForm.value.email, password: this.loginForm.value.password});
    this.router.navigate(['account']);
  }
}
