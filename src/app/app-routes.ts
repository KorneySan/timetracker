import { Routes } from '@angular/router';

import { HomePageComponent/*, AccountPageComponent*/ } from './pages';
import { AuthGuardService } from './services';

export const routes: Routes = [
    { 
        path: '', 
        redirectTo: 'home', 
        pathMatch: 'full' 
    }, //localhost -> home
    { 
        path: 'home', 
        component: HomePageComponent 
    },
    { 
        path: 'account', 
        /*component: AccountPageComponent, */
        loadChildren: './pages/account-page/account-page.module#AccountPageModule',
        canActivate: [AuthGuardService] 
    }
    //{ path: '**', component: PageNotFoundComponent },
];
